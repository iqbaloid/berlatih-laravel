<!DOCTYPE html>
<html>
	<head>
		<title>FORM</title>
	</head>
	<body>
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h2>
		<form action="{{ url('welcome') }}" method="POST">
			@csrf
			<table>
				<tr>
					<td><label>First name:</label></td>
				</tr>
				<tr>
					<td><input type ="text" name="fname" id="fname"></td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td><label>Last name:</label></td>
				</tr>
				<tr>
					<td><input type ="text" name="lname" id="lname"></td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td><label>Gender:</label></td>
				</tr>
				<tr>
					<td><input type ="radio" name="gender">Male</td>
				</tr>
				<tr>
					<td><input type ="radio" name="gender">Female</td>
				</tr>
				<tr>
					<td><input type ="radio" name="gender">Other</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td><label>Nationality:</label></td>
				</tr>
				<tr>
					<td><select name="national">
							<option value="Indonesia">Indonesian</option>
							<option value="Singapore">Singaporian</option>
							<option value="Malaysia">Malaysian</option>
							<option value="Australia">Australian</option>	
						</select>
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td><label>Language Spoken:</label></td>
				</tr>
				<tr>
					<td><input type="checkbox">Bahasa Indonesia</td>
				</tr>
				<tr>
					<td><input type="checkbox">English</td>
				</tr>
				<tr>
					<td><input type="checkbox">Other</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td><label>Bio:</label></td>
				</tr>
				<tr>
					<td><textarea name="bio" id="" cols="35" rows="10"></textarea></td>
				</tr>
				
				<tr><td><br></td></tr>
				<tr>
					<td><input type="submit" value="Sign Up"></td>
				</tr>
				
			</table>
		</form>
	</body>
	
</html>